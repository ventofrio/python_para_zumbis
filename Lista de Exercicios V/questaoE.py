__author__ = 'suporte'
contador = 0


def doisDigitosConsecutivosIguais(telefone):
    i = 0
    while i < len(telefone):
        if not i == len(telefone)-1 and telefone[i] == telefone[i+1]:
            return True
        i+=1
    return False



def somaEhPar(telefone):
    soma = 0
    for digito in telefone:
        soma += int(digito)
    if soma % 2 == 0:
        return True
    return False


def ultimoDigitoEhIgual(telefone):
    if telefone[0] == telefone[len(telefone)-1]:
        return True
    return False


with open('telefones.txt') as telefones:
    for linha in telefones.readlines():
        linha = str.rstrip(linha)
        for telefone in linha.split(' '):
            if not doisDigitosConsecutivosIguais(telefone) and somaEhPar(telefone) and not ultimoDigitoEhIgual(telefone):
                contador+=1
print(contador)
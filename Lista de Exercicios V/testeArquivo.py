__author__ = 'suporte'


def ip_ok(ip):
    ip = ip.split('.')
    for byte in ip:
        if int(byte) > 255:
            return False
    return True


with open('ips.txt') as ips:
    with open('validos.txt','w') as validos:
        with open('invalidos.txt','w') as invalidos:
            for ip in ips.readlines():
                if (ip_ok(ip)):
                    validos.write(ip)
                else:
                    invalidos.write(ip)